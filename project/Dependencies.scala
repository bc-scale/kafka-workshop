import sbt._

object Versions {
  val scalaVersion = "2.12.6"

  val kafkaVersion             = "2.0.0"
  val embeddedKafkaVersion     = "1.1.1"
  val kafkaStreamsQueryVersion = "0.1.1"
  val akkaVersion              = "2.5.14"
  val akkaHttpVersion          = "10.1.3"
  val akkaStreamKafkaVersion   = "0.22"

  val alpakkaVersion = "0.18"

  val configVersion = "1.3.3"

  val scalaLoggingVersion = "3.9.0"
  val slf4jVersion        = "1.7.25"
  val logbackVersion      = "1.2.3"

  val circeVersion = "0.9.3"

  val scalaTestVersion = "3.0.5"
}

object Dependencies {
  // scalastyle:off

  import Versions._

  val Resolvers: Seq[Resolver] =
    DefaultOptions.resolvers(snapshot = true) ++ Seq(
      Resolver.typesafeRepo("releases"),
      Resolver.jcenterRepo
    )

  object Akka {

    val akkaHttp   = "com.typesafe.akka" %% "akka-http"   % akkaHttpVersion
    val akkaActor  = "com.typesafe.akka" %% "akka-actor"  % akkaVersion
    val akkaStream = "com.typesafe.akka" %% "akka-stream" % akkaVersion
    val akkaSlf4j  = "com.typesafe.akka" %% "akka-slf4j"  % akkaVersion

    val akkaStreamKafka = "com.typesafe.akka" %% "akka-stream-kafka" % akkaStreamKafkaVersion

    val alpakkaCsv = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % alpakkaVersion
  }

  object Kafka {
    // official kafka libs
    val kafkaClients = "org.apache.kafka" % "kafka-clients" % kafkaVersion exclude ("org.slf4j", "slf4j-log4j12")
    val kafkaStreams = "org.apache.kafka" % "kafka-streams" % kafkaVersion exclude ("org.slf4j", "slf4j-log4j12")

    val kafka = "org.apache.kafka" %% "kafka" % kafkaVersion excludeAll (ExclusionRule(
      "org.slf4j",
      "slf4j-log4j12"
    ), ExclusionRule("org.apache.zookeeper", "zookeeper"))
    // kafka-streams-scala libs
    val kafkaStreamsScala = "org.apache.kafka" %% "kafka-streams-scala" % kafkaVersion
    val kafkaStreamsQuery = "com.lightbend"    %% "kafka-streams-query" % kafkaStreamsQueryVersion
  }

  object ConfigDeps {

    val all = Seq("com.typesafe" % "config" % configVersion)
  }

  object Circe {

    val all = Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser"
    ).map(_ % circeVersion)
  }

  object Testing {
    val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVersion
    val scalactic = "org.scalactic" %% "scalactic" % scalaTestVersion % Test

    val scalaTestDeps = Seq(scalaTest % Test, scalactic)

    val scalaTestKafka        = "net.manub"         %% "scalatest-embedded-kafka"         % embeddedKafkaVersion exclude ("log4j", "log4j")
    val scalaTestKafkaStreams = "net.manub"         %% "scalatest-embedded-kafka-streams" % embeddedKafkaVersion exclude ("log4j", "log4j")
    val akkaStreamTestKit     = "com.typesafe.akka" %% "akka-stream-testkit"              % akkaVersion
  }

  object Logging {
    val scalaLogging   = "com.typesafe.scala-logging" %% "scala-logging"   % scalaLoggingVersion
    val slf4j          = "org.slf4j"                  % "slf4j-api"        % slf4jVersion
    val log4jOverSlf4j = "org.slf4j"                  % "log4j-over-slf4j" % slf4jVersion
    val logback        = "ch.qos.logback"             % "logback-classic"  % logbackVersion

    val all = Seq(scalaLogging, slf4j, log4jOverSlf4j, logback)
  }

}
