package net.scalytica.kafkaworkshop.shared

final case class Region(code: String, name: String)

object Region {

  val empty: Region = Region("", "")

}

final case class Household(code: String, name: String)

final case class Stats(
    region: Region,
    household: Household,
    year: Int,
    count: Int
)

final case class HouseholdStats(
    household: Household,
    year: Int,
    count: Int
)

final case class AggregatedStats(
    region: Region,
    yearlyStats: Seq[HouseholdStats]
)

object AggregatedStats {

  val empty: AggregatedStats = null // scalastyle:ignore

}
