package net.scalytica.kafkaworkshop.producer

import java.nio.charset.StandardCharsets
import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.alpakka.csv.scaladsl.CsvParsing
import akka.stream.scaladsl.{FileIO, Flow}
import akka.util.ByteString
import akka.{Done, NotUsed}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import net.scalytica.kafkaworkshop.shared._
import net.scalytica.kafkaworkshop.shared.serdes.CirceSerializer
import org.apache.kafka.clients.producer.{ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.Serdes
import io.circe.generic.auto._
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Try

object ProducerApp extends App with LazyLogging {

  implicit val as  = ActorSystem("producer-app-system")
  implicit val mat = ActorMaterializer()
  implicit val ec  = as.dispatcher

  val producer = new DataProducer()

  logger.info("Starting producer...")

  val eventuallyDone = producer.start.map { _ =>
    logger.info("Producer completed writing data to kafka.")
  }.recover {
    case ex: Throwable =>
      logger.error(s"Producer failed with exception", ex)
  }

  Await.result(eventuallyDone, 2 minutes)

  logger.info("Terminating actor system...")

  mat.shutdown()
  as.terminate()

  logger.info("Actor system terminated. Stopping application")
}

class DataProducer()(
    implicit
    as: ActorSystem,
    mat: ActorMaterializer
) extends LazyLogging {

  val cfg       = ConfigFactory.load().getConfig("kafkaworkshop.kafka")
  val kafkaUrl  = cfg.asString("url")("localhost:29092")
  val clientId  = cfg.asString("clientId")("workshop-producer")
  val topicName = cfg.asString("topic")("workshop-input-topic")

  private[this] val valueStrEncoding = StandardCharsets.UTF_8

  private[this] val sourceFile =
    Paths.get(getClass.getResource("/HouseholdsByType2yrs.csv").toURI)

  private[this] val producerConfig =
    ProducerSettings(
      system = as,
      keySerializer = Serdes.String().serializer(),
      valueSerializer = new CirceSerializer[Stats]
    ).withProperties(ProducerConfig.CLIENT_ID_CONFIG -> clientId)
      .withBootstrapServers(kafkaUrl)

  def start: Future[Done] =
    FileIO
      .fromPath(sourceFile)
      .via(CsvParsing.lineScanner(CsvParsing.SemiColon))
      // Include the below throttle line produce elements at a slower rate
//      .throttle(1, 1 second)
      .via(logRawData)
      // First row is a header row we don't care about.
      .drop(1)
      .map(parseRow)
      .collect { case Some(stats) => stats }
      .map { stats =>
        new ProducerRecord[String, Stats](
          topicName,
          stats.region.code,
          stats
        )
      }
      .runWith(Producer.plainSink(producerConfig))

  private[this] def parseRow(row: List[ByteString]): Option[Stats] =
    row match {
      case _ @region :: household :: y :: _ :: numHouseholds :: Nil =>
        for {
          (regCode, regName) <- safeDecode(region).map(splitString)
          (hhCode, hhName)   <- safeDecode(household).map(splitString)
          year               <- safeDecode(y)
          count              <- safeDecode(numHouseholds)
        } yield {
          Stats(
            region = Region(regCode, regName),
            household = Household(hhCode, hhName),
            year = year.toInt,
            count = count.toInt
          )
        }

      case values =>
        logger.warn(s"Unexpected length of values $values")
        None
    }

  private[this] def splitString(s: String): (String, String) =
    s.splitAt(s.indexOf(" "))

  private[this] def safeDecode(s: ByteString): Option[String] = {
    Try(s.decodeString(valueStrEncoding)).toOption.flatMap {
      case ""    => None
      case value => Some(value)
    }
  }

  private[this] def logRawData
    : Flow[List[ByteString], List[ByteString], NotUsed] =
    Flow[List[ByteString]].map { s =>
      logger.debug(
        "Processing raw data: " +
          s"${s.map(_.decodeString(valueStrEncoding)).mkString(";")}"
      )
      s
    }

}
